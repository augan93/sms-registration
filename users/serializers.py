from rest_framework import serializers
from . import models
from django.contrib.auth.models import User
from django.contrib.auth import password_validation
from django.utils import timezone
from datetime import timedelta


class GetCodeSerializer(serializers.ModelSerializer):
    """Send phone to get code for registration """

    class Meta:
        model = models.PhoneCode
        fields = ['phone']

    def validate_phone(self, value):
        value = value.lower()

        if User.objects.filter(username=value).exists():
            raise serializers.ValidationError({
                "error": "User with this phone number exists",
                "code": "PHONE_EXISTS"
            })
        return value


class CheckCodeSerializer(serializers.ModelSerializer):
    """Send phone number and code for validation"""

    class Meta:
        model = models.PhoneCode
        fields = ['phone', 'code']

    def validate(self, data):
        phone = data['phone'].lower()
        code = data['code']

        try:
            phone_code = models.PhoneCode.objects.filter(phone=phone).latest('id')
        except models.PhoneCode.DoesNotExist:
            raise serializers.ValidationError({
                "error": "No code sent to this phone number",
                "code": "NO_CODE_SENT"
            })

        if len(str(code)) != 6:
            raise serializers.ValidationError({
                "error": "Length of code is not equal to 6",
                "code": "INVALID_CODE_LENGTH"
            })

        if not phone_code.code == code:
            raise serializers.ValidationError({
                "error": "Invalid code",
                "code": "INVALID_CODE",
            })

        if timezone.now() - phone_code.created_dt > timedelta(minutes=30):
            raise serializers.ValidationError({
                "error": "Code expired",
                "code": "EXPIRED_CODE",
            })

        phone_code.is_verified = True
        phone_code.save()

        return data


class UserCreateSerializer(serializers.ModelSerializer):
    password = serializers.CharField(style={'input_type': 'password'})
    password2 = serializers.CharField(style={'input_type': 'password'})
    phone = serializers.CharField()
    name = serializers.CharField()

    class Meta:
        model = User
        fields = [
            'name',
            'password',
            'password2',
            'phone',
        ]

    def validate(self, data):
        if data['password'] != data['password2']:
            raise serializers.ValidationError({
                "error": "Passwords don't match",
                "code": "PASSWORD_MISMATCH"
            })
        password2 = data['password2']
        password_validation.validate_password(password2)
        return data

    def validate_phone(self, value):
        value = value.lower()

        if User.objects.filter(username=value).exists():
            raise serializers.ValidationError({
                "error": "User with this phone number exists",
                "code": "PHONE_EXISTS"
            })

        try:
            phone_code = models.PhoneCode.objects.filter(phone=value).latest('id')
        except models.PhoneCode.DoesNotExist:
            raise serializers.ValidationError({
                "error": "No code sent to this phone number",
                "code": "NO_CODE_SENT"
            })

        if not phone_code.is_verified:
            raise serializers.ValidationError({
                "error": "Your phone is not verified",
                "code": "NON_VERIFIED_PHONE",
            })

        if timezone.now() - phone_code.created_dt > timedelta(minutes=30):
            raise serializers.ValidationError({
                "error": "Code expired",
                "code": "EXPIRED_CODE"
            })

        return value

    @staticmethod
    def extract_names(name):
        name_list = name.split(' ')
        if len(name_list) > 1:
            return name_list[0], name_list[1]
        return name_list[0], ""

    def create(self, validated_data):
        phone = validated_data['phone'].lower()
        name = validated_data['name']
        first_name, last_name = self.extract_names(name)

        user = User(
            username=phone,
            first_name=first_name,
            last_name=last_name
        )

        user.set_password(validated_data['password'])
        user.save()

        return user
