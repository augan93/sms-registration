import random

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from . import models
from . import serializers
from .tasks import send_code


class GetCodeView(APIView):
    permission_classes = ()
    authentication_classes = ()

    def post(self, request):
        """
        Get code for registration.
        Send 'phone' as POST parameter
        """
        serializer = serializers.GetCodeSerializer(data=request.data)
        if serializer.is_valid():
            code = str(random.randint(100000, 999999))
            cd = serializer.validated_data
            phone = cd['phone'].lower()

            message = 'Your confirmation code: ' + str(code)
            send_code.delay(phone, message)

            models.PhoneCode.objects.create(
                code=code,
                phone=phone,
            )

            return Response({"success": True}, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CheckCodeView(APIView):
    permission_classes = ()
    authentication_classes = ()

    def post(self, request):
        """
        Send code for validation.
        Pass 'code' and 'phone' as POST parameters.
        """
        serializer = serializers.CheckCodeSerializer(data=request.data)
        if serializer.is_valid():
            return Response({"success": True}, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class RegisterView(APIView):
    permission_classes = ()
    authentication_classes = ()

    def post(self, request):
        """
        Send 'password', 'password2', 'phone', 'name' as POST parameters
        to register user
        """
        serializer = serializers.UserCreateSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()

            return Response({"success": True},
                            status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
