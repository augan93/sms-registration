from django.urls import path

from . import views

app_name = 'users'

urlpatterns = [
    path('get-code/', views.GetCodeView.as_view(), name='api-get-code'),
    path('check-code/', views.CheckCodeView.as_view(), name='check-code'),
    path('register/', views.RegisterView.as_view(), name='api-register'),
]
