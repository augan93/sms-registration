import requests
from sms_registration.celery import app
from django.conf import settings


@app.task(autoretry_for=(Exception,), retry_backoff=2)
def send_code(phone, message):
    phone = str(phone)
    if phone[0] == "+":  # MOBIZON API requirements
        phone = phone[1:]

    response = requests.post(
        f"https://api.mobizon.kz/service/Message/SendSmsMessage?apiKey={settings.MOBIZON_API_KEY}",
        data={
            "recipient": phone,
            "text": message,
            "from": "Test app",
        }
    )
