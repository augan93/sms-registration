from django.db import models


class PhoneCode(models.Model):
    phone = models.CharField(
        max_length=16,
        db_index=True,
    )
    code = models.IntegerField()
    is_verified = models.BooleanField(default=False)
    created_dt = models.DateTimeField(auto_now_add=True)
