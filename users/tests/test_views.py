from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from django.contrib.auth.models import User
from unittest.mock import patch

from users import models


class GetCodeViewTest(APITestCase):
    data = {
        "phone": "+770711111111"
    }
    url = reverse('users:api-get-code')

    @patch('users.tasks.send_code.delay')
    def test_get_code_success(self, send_code_mock_delay):
        response = self.client.post(self.url,
                                    data=self.data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(
            models.PhoneCode.objects.filter(phone=self.data["phone"]).exists()
        )
        self.assertTrue(
            send_code_mock_delay.called
        )

        phone_code = models.PhoneCode.objects.first()
        send_code_mock_delay.assert_called_with(
            self.data["phone"],
            'Your confirmation code: ' + str(phone_code.code)
        )

    def test_get_code_failed(self):
        """Requesting code should fail because, the phone is already registered."""
        User.objects.create(
            username=self.data["phone"],
        )
        response = self.client.post(self.url,
                                    data=self.data,
                                    format='json')
        self.assertEqual(
            response.status_code,
            status.HTTP_400_BAD_REQUEST
        )


class CheckCodeViewTest(APITestCase):
    url = reverse('users:check-code')
    data = {
        "phone": "+770711111111",
        "code": 777777
    }

    def setUp(self):
        models.PhoneCode.objects.create(
            phone=self.data["phone"],
            code=777777,
            is_verified=False,
        )

    def test_check_code_success(self):
        response = self.client.post(self.url,
                                    data=self.data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        phone_code = models.PhoneCode.objects.filter(phone=self.data["phone"]).latest("id")
        self.assertTrue(phone_code.is_verified)

    def test_check_fails_invalid_code(self):
        data = {
            "phone": "+770711111111",
            "code": 666666
        }
        response = self.client.post(self.url,
                                    data=data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json()['code'][0],
            "INVALID_CODE"
        )

    def test_check_fails_code_not_sent(self):
        data = {
            "phone": "+7707XXXXXXX",  # No code sent to this phone
            "code": 777777
        }
        response = self.client.post(self.url,
                                    data=data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json()['code'][0],
            "NO_CODE_SENT"
        )

    def test_check_fails_wrong_code_length(self):
        data = {
            "phone": "+770711111111",
            "code": 777
        }
        response = self.client.post(self.url,
                                    data=data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json()['code'][0],
            "INVALID_CODE_LENGTH"
        )


class RegisterViewTest(APITestCase):
    url = reverse('users:api-register')
    data = {
        "phone": "+770711111111",
        "name": "John Snow",
        "password": "strong_psw2022",
        "password2": "strong_psw2022"
    }

    def setUp(self):
        self.phone_code = models.PhoneCode.objects.create(
            phone=self.data["phone"],
            code=777777,
            is_verified=True,
        )

    def test_fails_phone_not_verified(self):
        self.phone_code.is_verified = False
        self.phone_code.save()

        response = self.client.post(self.url,
                                    data=self.data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json()['phone']['code'],
            'NON_VERIFIED_PHONE'
        )

    def test_register_success(self):
        response = self.client.post(self.url,
                                    data=self.data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        user = User.objects.filter(username=self.data["phone"]).first()
        self.assertTrue(user)
        self.assertEqual(
            user.first_name,
            self.data["name"].split(" ")[0],
        )
        self.assertEqual(
            user.last_name,
            self.data["name"].split(" ")[1],
        )

    def test_fails_password_mismatch(self):
        self.data['password2'] = "wrong_password"
        response = self.client.post(self.url,
                                    data=self.data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json()['code'][0],
            'PASSWORD_MISMATCH'
        )
        self.data['password2'] = "strong_psw2022"  # change to the proper password

    def test_fails_user_already_exists_with_phone_number(self):
        User.objects.create(
            username=self.data["phone"],
        )
        response = self.client.post(self.url,
                                    data=self.data,
                                    format='json')
        self.assertEqual(
            response.status_code,
            status.HTTP_400_BAD_REQUEST
        )
        self.assertEqual(
            response.json()['phone']['code'],
            'PHONE_EXISTS'
        )
