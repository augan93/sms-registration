from django.contrib import admin

from . import models


@admin.register(models.PhoneCode)
class PhoneCodeAdmin(admin.ModelAdmin):
    list_display = (
        'phone',
        'code',
        'is_verified',
        'created_dt',
    )
