## sms-registration functionality

1. Phone number is saved in User.username field as this field is unique
2. For sending SMS Mobizon platform is used. https://mobizon.kz/
3. Celery is used for sending requests to SMS platform

## Endpoints:
1. /api/v1/users/get-code/

    METHOD: POST
    
    BODY JSON:
    
        {
            "phone": "8777XXXXXXX"
        }

2. /api/v1/users/check-code/

    METHOD: POST
    
    BODY in JSON
    
        {
            "phone": "8777XXXXXXX",
            "code": 520528
        }
    
3. /api/v1/users/register/

    METHOD: POST
    
    BODY JSON:
    
        {
            "phone": "8777XXXXXXX",
            "name": "John Snow",
            "password": "password",
            "password2": "pasword"
        }

## Run test

    python manage.py test
    
    OR
    
    pytest

## Run project:

    python3 -m venv venv
    
    pip install -r requirements.txt
    
    python manage.py migrate
    
    python manage.py runserver
    
    Start Celery worker: 
    
    celery -A sms_registration worker --loglevel=info